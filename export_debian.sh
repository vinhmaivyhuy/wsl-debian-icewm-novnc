#!/bin/bash
set -e

echo Docker process need to be started
docker pull debian
docker container prune --force
docker run -t debian date
dockerContainerID=$(docker container ls -a | grep -i debian | awk '{print $1}')
docker export $dockerContainerID > /mnt/c/Temp/debian.tar

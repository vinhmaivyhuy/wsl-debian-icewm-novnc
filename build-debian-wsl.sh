#!/bin/bash

set -e

# Update the package manager and upgrade the system
# #################################################
apt-get -y update
apt-get -y upgrade
apt-get -y install net-tools passwd bzip2 sudo wget vim 
apt-get -y install tini supervisor
apt-get -y install openssh-server openssh-client
apt-get -y install locales locales-all 
apt-get -y install git tcl tk make
mkdir -p /var/run/sshd && sed -i "s/UsePrivilegeSeparation.*/UsePrivilegeSeparation no/g" /etc/ssh/sshd_config \
	 && sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config
ssh-keygen -A

# Set the locale
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8


# Install icewm and tightvnc server.
# #################################################
apt-get -y install xorg xauth xinit xfonts-base 
apt-get -y install icewm
apt-get -y install tigervnc-standalone-server tigervnc-common 
apt-get -y install xterm firefox-esr
apt-get clean all


# install and setup noVNC
# #################################################
apt-get -y install novnc
apt-get -y install websockify
ln -sf /usr/share/novnc/vnc_lite.html /usr/share/novnc/index.html


# Setup Supervisord
# #################################################
cp ./supervisor.default /etc/default/supervisor
cp -R ./supervisor/* /etc/supervisor/
cp supervisor.sh /root/
chmod u+x /root/run.sh
echo 'export DISPLAY=:1' >> /root/.bashrc

# Set up User (debianuser)
# #################################################
if [[ ! -d "/home/debianuser" ]]; then
 useradd -s /bin/bash -m -b /home debianuser
 echo 'export DISPLAY=:1' >> /home/debianuser/.bashrc
fi
touch /home/debianuser/.Xauthority
chmod go-rwx /home/debianuser/.Xauthority
mkdir -p /home/debianuser/.vnc
chown -R debianuser:debianuser /home/debianuser

# Set up User debianadmin
# #################################################
if [[ ! -d "/home/debianadmin" ]]; then
 useradd -s /bin/bash -m -b /home debianadmin
 echo 'export DISPLAY=:1' >> /home/debianadmin/.bashrc
 echo "debianadmin  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/debianadmin
 mkdir /home/debianadmin/.ssh
 cp authorized_keys /home/debianadmin/.ssh/authorized_keys
fi
chown -R debianadmin:debianadmin /home/debianadmin/.ssh
chmod -R go-rwx /home/debianadmin/.ssh
cp wsl.conf /etc

# Finalize installation and default command
# #################################################
#mkdir /tmp/.X11-unix
#chmod 1777 /tmp/.X11-unix

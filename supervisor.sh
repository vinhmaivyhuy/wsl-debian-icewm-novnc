#!/bin/bash
set -e

## Daemon
#/usr/bin/supervisord -c /etc/supervisord.conf

## Shell
#if [[ ! -d "/tmp/.X1-lock" ]]; then
export HOME=/home/debianuser
export USER=debianuser
cd /$HOME
exec /usr/bin/supervisord -n -c /etc/supervisor/supervisord.conf
#fi
